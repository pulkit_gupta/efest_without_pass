var httpProxy = require('http-proxy') ; 

var options = {
  hostnameOnly: true,
  router: {
    'efest.in': '127.0.0.1:3000',
    'www.efest.in': '127.0.0.1:3000',
    'neuroscada.com': '127.0.0.1:1000',
    'www.neuroscada.com': '127.0.0.1:1000',
    'ecelebrate.net': '127.0.0.1:4000',
    'www.ecelebrate.net': '127.0.0.1:4000',
  }
}
var proxyServer = httpProxy.createServer(options);
proxyServer.listen(80,function(){
	console.log("proxy server running on port 80 which is serving child servers");
});

