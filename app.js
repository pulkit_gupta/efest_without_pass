
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')  
  , http = require('http')
  , path = require('path')
  , member = require('./routes/member')
  , io     = require('socket.io')
  , fs =require('fs');
  
var util = require('util'); 
var config = require('./config').Config; 

var app = express();
var server = http.createServer(app);

var Memstore = express.session.MemoryStore;
var RedisStore = require ( 'connect-redis' ) ( express ),
sessionStore = new RedisStore ();

var logFile = fs.createWriteStream('./efesthttp.log',{flags: 'a'});

app.configure(function(){  
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  //app.use(express.favicon());
  app.use(express.favicon(__dirname + '/public/favicon.ico', { maxAge: 2592000000 }));  
  app.use(express.logger({stream: logFile}));
  app.use(express.bodyParser());
  app.use(express.methodOverride());  
  app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(express.cookieParser()); 
  
  if(config.env=="development"){
  	app.use(express.session({ secret: 'somerandomstring', store: Memstore({
  	reapInterval: 60000*10  
  	})}));
  }
  else{
  	app.use ( express.session ( {
      secret: "keyboard cat", store: sessionStore, key: 'hello.sid'
   } ) );
  }
  app.use(app.router);
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

var mysql = require('mysql');
var connection;
reconnect();
function reconnect()
{
	console.log('Trying to connect sql. in app.js');
	connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
      password : config.dbPassword,
      database : config.dbName
  	});  	
  	connection.connect(function(err)
	{
		if(err)
		{
			console.log("failed");
			reconnect();
		}
		else
		{
			console.log("connected");
			connection.on('error', function(err) 
			{
			   /* if (!err.fatal) {
			      return;
			    }*/
				if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
			      //throw err;
			      console.log("Error is coontinous. and can not be solved.");
			    }
			    else
			    {
			    	console.log("Retrying!!unexpected connection lost.");
			    	reconnect();
			    }    
			});

		}

	});	    
}
  
  
function requiresLogin(req, res, next){
	if(req.session.user){
	console.log("session is there");
	next();
	}
	else{
	console.log("session is not there");
	res.redirect('/login?redir='+req.url);
	
	}
}
/* Controllers //////////////////////////////////////////////////////////////////////////////////////////////////*/

app.get('/', routes.index);
app.get('/browser', routes.browser);
app.get('/steps', routes.steps);
app.get('/how', routes.how);
app.get('/terms', routes.terms);
app.get('/contact', routes.contact);
app.get('/register', user.signup);
app.post('/userconfirm', user.confirm);  
app.get('/activate', user.activate);
app.post('/loginuser', user.loginuser);
app.get('/login', user.login);
app.get('/logout', function(req, res){
	if(req.session.user)
	delete req.session.user;
	
	res.redirect('login');
});
app.get('/forgot', member.forgot);
app.post('/forgotprocess', member.forgotprocess);
app.get('/resetpassword', member.resetpassword);   //a onetime link
app.post('/newpassword', member.newpassword);

app.get('/member',requiresLogin, member.page);
app.get('/settings',requiresLogin, member.settings);
app.post('/update',requiresLogin, user.update);
app.get('/mycelebrations', requiresLogin, member.mycelebrations);

app.get('/plans',member.plans);
app.get('/gift', requiresLogin, member.gift);
app.get('/giftcoupon',routes.giftcoupon);
app.post('/pay',member.pay);
app.post('/payforgift',member.payforgift);

app.get('/status',member.status);
app.get('/statusfail', member.statusfail);  
app.get('/statusgift',member.statusgift);
app.get('/statusgiftfail',member.statusgiftfail);

app.get('/play',member.play);

function common (req, res, next) {
	console.log("get request");
  if (req.headers.host.match(/^www/) !== null ) {
    res.redirect('http://' + req.headers.host.replace(/^www\./, '') + req.url);
  } else {
    next();     
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
io = io.listen(server);	
io.set('log level', 1);

server.listen(config.socketPort, function(){
  console.log("Express server listening on port " + config.socketPort);        
  console.log("environment started is "+config.env);
  console.log("URL is "+config.appURL);    
  console.log("Time zone with respect to UTC is "+config.timeZone);    
 //testing new code   	     
  // testing over
});


function destroySocket(gameid)
{
	var date = new Date();
	date.setTime(date.getTime()+config.timeZone*60*60*1000);  //timestamp		
	var end_at = date.toUTCString();
	
	var sql2='UPDATE games SET status="Expired" , ended_at='+connection.escape(end_at)+' WHERE gameid='+connection.escape(gameid);
	connection.query(sql2, function(err, result2){
		if(err)
		console.log(err);
		console.log("Database Done");								
		io.sockets.emit(gameid+"chat", "Sever", "Your Session is over. :) You will be redirected in 7 Secs");
		io.sockets.emit(gameid+"disconnect","serverorder");
	});
	
}


function clock(secs, gameid)
{	

	if(secs>0)
	{
		var sec1=secs%60;
		if(sec1<10)
		sec1="0"+sec1;
		var min1 =parseInt(secs/60); 		
		io.sockets.emit(gameid+"info",{event: "time", val: {min: min1, sec: sec1}});				
		
		setTimeout(function(){
		clock(secs-1,gameid);
	
		},1000);		
		
	}
	else if(secs<=0)
	{
		for(i=0;i<startedGames.length;i++)
		{
			if(startedGames[i]==gameid)
			{
				console.log("deleting game from active memory");
				delete startedGames[i];
				startedGames.splice(i,1);					
				break;
			}
		}			
		destroySocket(gameid);	
		
	}
		
}


var liveUsers = Array();
var startedGames = Array();

function isGameStarted(gameid,callback)
{
	for(var i=0;i<startedGames.length;i++)
	{
		if(startedGames[i]==gameid)
		{
			return 0;
		}
	}
	startedGames.push(gameid);
	console.log("game is new hence started");
	callback();
}

io.sockets.on('connection', function (socket) {
	console.log("Some one has connected, waiting for information"); 	
 	socket.on("addmygame", function (data) {			 		
		console.log("started game with gameid = "+data.gameid);     	 			
		socket.user = data;														
		isGameStarted(data.gameid, function(){clock(data.timersec,data.gameid);});
		
		console.log("socket name is"+socket.user.email+" and its timer value is"+socket.user.timersec);     	 	
		
		socket.on(data.gameid, function(status){	
		
			console.log("Data is recieved from"+data.email); 					
 			console.log("emiting events to all user of game id="+data.gameid);					 	
 			if(status.event=="feedback")
 			{
 				socket.broadcast.emit(data.gameid+"chat", "Server", socket.user.name+"'s system is ready for next cracker"); 				
 			}
 			else if(status.event=="fireRocket"||status.event=="fireAnar")
 			{
 				io.sockets.emit(data.gameid,{event: status.event}); 			
 				socket.broadcast.emit(data.gameid+"chat", "Server", socket.user.name+" has fired a cracker"); 				
 			}
 			else if(status.event=="pickStick")
 			{
				socket.broadcast.emit(data.gameid+"chat", "Server","Attention! "+socket.user.name+" has picked sparkle stick and going to fir the cracker. Stop playing with your sticks!");				 			
 			}
 			else if(status.event=="setcracker")
 			{
 				var arr = new Array(); 				 		
 				for(i=0;i<350;i++)
 				{arr[i]=Math.random();} 					 			
 				io.sockets.emit(data.gameid,{event: "setcracker", type: status.type, randval: arr}); 			 			
 				socket.broadcast.emit(data.gameid+"chat", "Server", socket.user.name+" has selected a cracker");
 			} 			
		});
		
		socket.on(data.gameid+"chat", function(username, msg){		 		
			console.log("Data is recieved from"+data.email); 					
		 	console.log("emiting events to all user of game id="+data.gameid);					 	
		 	io.sockets.emit(data.gameid+"chat",username, msg);     	         	 
		 					
		}); 	 
		//Notify yourself of all user
		socket.emit(data.gameid+"chat", "Server", "You are Connected");
		
		
		liveUsers.push(socket);
		
		
		for(i=0;i<liveUsers.length;i++)
		{
			if(liveUsers[i].user.gameid==data.gameid)
			{
				if(liveUsers[i].user.email!=data.email)
				socket.emit(data.gameid+"info",{event: "photo",mail: liveUsers[i].user.email, action: "come"});				;
				console.log("users present are"+liveUsers[i].user.email);
			}
		}
		//update another users online
		socket.broadcast.emit(data.gameid+"chat", "Server", socket.user.name+" is Connected");
		
		//usernames; update with the connecting one			
		socket.broadcast.emit(data.gameid+"info",{event: "photo",mail: socket.user.email, action: "come"});				
	
	}); //adduser			   	 
	
	socket.on("disconnect", function (data) {
		if(socket.user)  //always true...because auth done by controller
		{	console.log(socket.user.email+"disconnected");
			socket.broadcast.emit(socket.user.gameid+"chat", "Server", socket.user.name+" is disconnected");					
			socket.broadcast.emit(socket.user.gameid+"info",{event: "photo",mail: socket.user.email, action: "gone"});										
			
			for(i=0;i<liveUsers.length;i++)
			{
				if(liveUsers[i].user.email==socket.user.email)
				{
					
					delete liveUsers[i];
					liveUsers.splice(i,1);					
					console.log("socket object deleted from live users");
					break;
				}
			}	
		}	
	});
	
});//main connect


/*process.on('uncaughtException', function (err) {
  util.log('Caught exception: ' + err.message);
});*/
 
 